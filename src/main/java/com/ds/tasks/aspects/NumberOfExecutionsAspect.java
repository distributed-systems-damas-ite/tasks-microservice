package com.ds.tasks.aspects;

import io.micrometer.core.instrument.MeterRegistry;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NumberOfExecutionsAspect {

    private final MeterRegistry meterRegistry;

    @Autowired
    public NumberOfExecutionsAspect(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @After("@annotation(com.ds.tasks.aspects.annotations.RegisterNumberOfExecutions)")
    public void logExecutionTime(JoinPoint joinPoint) {
        // example: TasksService.create
        this.meterRegistry.counter(joinPoint.getSignature().toShortString().split("\\(")[0]).increment();
    }
}
