package com.ds.tasks.aspects;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class RegisterExecutionTimeAspect {

    private final MeterRegistry meterRegistry;

    @Autowired
    public RegisterExecutionTimeAspect(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Pointcut("within(@com.ds.tasks.aspects.annotations.RegisterExecutionTime *)")
    public void withinRegisterExecutionTime(){}

    @Around("withinRegisterExecutionTime()")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {

        // example: TasksService.create.timer
        String method = joinPoint.getSignature().toShortString().split("\\(")[0] + ".timer";
        Timer timer = this.meterRegistry.timer(method);
        long oldTimer = meterRegistry.get(method).timer().count();

        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;

        if (executionTime - oldTimer > 0000.1)
            timer.record(executionTime, TimeUnit.MILLISECONDS);

        return proceed;
    }
}
