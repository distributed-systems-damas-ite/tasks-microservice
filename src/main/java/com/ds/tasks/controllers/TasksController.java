package com.ds.tasks.controllers;

import com.ds.tasks.domain.Task;
import com.ds.tasks.dto.CreateTaskDTO;
import com.ds.tasks.dto.DeleteTasksRequestDto;
import com.ds.tasks.dto.EstimateTaskResponseDTO;
import com.ds.tasks.dto.UpdateTaskDTO;
import com.ds.tasks.services.TasksService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("tasks")
public class TasksController {

    private TasksService tasksService;

    @Autowired
    public TasksController(TasksService tasksService) {
        this.tasksService = tasksService;
    }

    @GetMapping()
    public List<Task> findAll(@RequestParam(required = false) Long projectId) {
        return this.tasksService.findAll(projectId);
    }

    @GetMapping(value = "/{id}")
    public Task findById(@PathVariable long id) {
        return this.tasksService.findById(id);
    }

    @PostMapping()
    public Task create(@Valid @RequestBody CreateTaskDTO task) {
        return this.tasksService.create(task);
    }

    @PostMapping(value = "estimate/{id}")
    public Task updateEstimation(@PathVariable long id, @RequestBody EstimateTaskResponseDTO dto) {
        return this.tasksService.updateEstimation(id, dto);
    }

    @PutMapping(value = "/{id}")
    public Task update(@PathVariable long id, @Valid @RequestBody UpdateTaskDTO task) {
        return this.tasksService.update(id, task);
    }

    @DeleteMapping(value = "/{id}")
    public boolean delete(@PathVariable long id) {
        return this.tasksService.delete(id);
    }

    @RabbitListener(queues = "${rabbitmq.projects.queue}")
    public void listen(DeleteTasksRequestDto requestDto) {
        tasksService.deleteByProjectId(requestDto.getProjectId());
    }

}
