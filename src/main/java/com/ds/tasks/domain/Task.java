package com.ds.tasks.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity()
@Table(name = "tasks")
public class Task {

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    @NotNull(message = "project id is required")
    private Long projectId;

    @Column()
    @NotBlank(message = "task name is required")
    @NotNull(message = "task name is required")
    private String name;

    @Column()
    private String description;

    @Column()
    @NotNull(message = "task user estimation is required")
    private Float userEstimation;

    @Column()
    private Float aiEstimation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }

    public Float getAiEstimation() {
        return aiEstimation;
    }

    public void setAiEstimation(Float aiEstimation) {
        this.aiEstimation = aiEstimation;
    }
}
