package com.ds.tasks.dto;

public class DeleteTasksRequestDto {

    private Long projectId;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}
