package com.ds.tasks.dto;

import com.ds.tasks.domain.Task;

public class EstimateTaskRequestDTO {

    private Long id;
    private Float userEstimation;

    public EstimateTaskRequestDTO(Long id, Float userEstimation) {
        this.id = id;
        this.userEstimation = userEstimation;
    }

    public EstimateTaskRequestDTO(Task task) {
        this.id = task.getId();
        this.userEstimation = task.getUserEstimation();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }
}
