package com.ds.tasks.repositories;

import com.ds.tasks.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    public  <S extends Task> List<S> findAllByNameOrDescription(String name, String description);

    public  <S extends Task> List<S> findAllByProjectId(Long projectId);

    public Integer deleteAllByProjectId(Long projectId);
}
