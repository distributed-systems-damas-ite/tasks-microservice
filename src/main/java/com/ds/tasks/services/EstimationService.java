package com.ds.tasks.services;

import com.ds.tasks.domain.Task;
import com.ds.tasks.dto.EstimateTaskRequestDTO;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EstimationService {

    @Value("${rabbitmq.tasks.exchange}")
    private String exchange;

    @Value("${rabbitmq.tasks.routingkey}")
    private String routingKey;

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public EstimationService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void estimate(Task task) {
        EstimateTaskRequestDTO estimateTaskRequestDTO = new EstimateTaskRequestDTO(task);
        this.rabbitTemplate.convertAndSend(exchange, routingKey, estimateTaskRequestDTO);
    }
}
