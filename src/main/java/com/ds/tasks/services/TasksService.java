package com.ds.tasks.services;

import com.ds.tasks.aspects.annotations.RegisterExecutionTime;
import com.ds.tasks.aspects.annotations.RegisterNumberOfExecutions;
import com.ds.tasks.domain.Task;
import com.ds.tasks.dto.CreateTaskDTO;
import com.ds.tasks.dto.EstimateTaskResponseDTO;
import com.ds.tasks.dto.UpdateTaskDTO;
import com.ds.tasks.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RegisterExecutionTime
public class TasksService {

    private TaskRepository taskRepository;
    private EstimationService estimationService;

    @Autowired
    public TasksService(TaskRepository taskRepository, EstimationService estimationService) {
        this.taskRepository = taskRepository;
        this.estimationService = estimationService;
    }

    public List<Task> findAll(Long projectId) {
        if (projectId != null) return this.findAllByProjectId(projectId);
        return this.taskRepository.findAll();
    }

    private List<Task> findAllByProjectId(Long projectId) {
        return this.taskRepository.findAllByProjectId(projectId);
    }

    public Task findById(Long id) {
        return this.taskRepository.findById(id).orElseThrow(() -> {
            return new ResponseStatusException(HttpStatus.NOT_FOUND); });
    }

    @RegisterNumberOfExecutions
    public Task create(CreateTaskDTO taskDto) {
        Task task = new Task();
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setProjectId(taskDto.getProjectId());
        task.setUserEstimation(taskDto.getUserEstimation());

        task =  this.taskRepository.save(task);
        this.estimationService.estimate(task);
        return task;
    }

    @RegisterNumberOfExecutions
    public Task update(Long id, UpdateTaskDTO taskDto) {
        Task task = this.findById(id);
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setProjectId(taskDto.getProjectId());
        task.setUserEstimation(taskDto.getUserEstimation());

        return this.taskRepository.save(task);
    }

    public Task updateEstimation(Long taskId, EstimateTaskResponseDTO estimateTaskResponseDTO) {
        Task task = this.findById(taskId);
        task.setAiEstimation(estimateTaskResponseDTO.getAiEstimation());
        return this.taskRepository.save(task);
    }

    @RegisterNumberOfExecutions
    public boolean delete(Long id) {
        Task task = this.findById(id);
        this.taskRepository.delete(task);
        return true;
    }

    @Transactional
    public boolean deleteByProjectId(Long projectId) {
        return this.taskRepository.deleteAllByProjectId(projectId) > 0;
    }
}
