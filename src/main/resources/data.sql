DROP TABLE IF EXISTS tasks;

CREATE TABLE tasks (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  project_id INTEGER,
  name VARCHAR(250) NOT NULL,
  description TEXT,
  user_estimation FLOAT NOT NULL,
  ai_estimation FLOAT
);

INSERT INTO tasks (project_id, name, description, user_estimation, ai_estimation) VALUES
  (1, 'Task 1', 'This is task 1 description' , 8, 10),
  (2, 'Task 2', null , 8, 10),
  (3, 'Task 3', null , 8, 10),
  (1, 'Task 4', null , 8, 10),
  (2, 'Task 5', null , 8, 10),
  (3, 'Task 6', null , 8, 10);